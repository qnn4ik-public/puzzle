from typing import Union


def unwrap_base(obj: Union[list, dict]) -> dict:
    if isinstance(obj, list):
        return {f'{i}': el for i, el in enumerate(obj)}
    else:
        return obj


def concat_keys(key: str, dct: dict) -> dict:
    return {f'{key}_{k}': v for k, v in dct.items()}


def unwrap(obj: Union[list, dict]) -> dict:
    res = unwrap_base(obj)

    while True:
        all_vals_str = 1

        for i, el in enumerate(res):
            key, val = el, res.get(el)

            if not isinstance(val, str):
                all_vals_str *= 0

            if isinstance(val, list):
                unwrapped_lst = unwrap_base(val)
                res.update({key: unwrapped_lst})
            elif isinstance(val, dict):
                unwrapped_dct = concat_keys(key, val)
                res.pop(key)
                res.update(unwrapped_dct)
                break  # not to change dict size

        if all_vals_str:
            break

    return res
