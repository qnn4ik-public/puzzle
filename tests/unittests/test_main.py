import pytest

from src.main import unwrap


@pytest.mark.parametrize(
    ['case', 'res'],
    [
        ({"a": "1"}, {"a": "1"}),
        (["1", "2"], {"0": "1", "1": "2"}),
        ({"a": {"b": "1"}}, {"a_b": "1"}),
        ({"a": ["1", "2"]}, {"a_0": "1", "a_1": "2"}),
        (["a", {"s": "1"}], {"0": "a", "1_s": "1"}),
        ({"a": [{"b": "1"}, {"c": "2"}]}, {"a_0_b": "1", "a_1_c": "2"}),
        ({"a": "1", "b": ["2", "3"]}, {"a": "1", "b_0": "2", "b_1": "3"})
    ]
)
def test_unwrap(case, res):
    assert unwrap(case) == res


